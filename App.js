import React, { Component } from 'react';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operator: '+',
      num1: '',
      num2: '',
      result: '0',
      Array: []
    };
    this.actionHandler = this.actionHandler.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this)
  }
  handleChange = (e) => {
    this.setState({ operator: e.target.value })
  }

  handleInputChange(e) {
    this.setState({
      [e.target.name]: Number(e.target.value)
    });
  }

  actionHandler = (e) => {
    e.preventDefault();
   
    if(this.state.operator==='+'){
      let x = this.state.num1 + this.state.num2;
      this.setState({ result: x })
    }
    if(this.state.operator==='-'){
      let x = this.state.num1 - this.state.num2;
      this.setState({ result: x })
    }
    if(this.state.operator==='*'){
      let x = this.state.num1 * this.state.num2;
      this.setState({ result: x })
    }
    if(this.state.operator==='/'){
      let x = this.state.num1 / this.state.num2;
      this.setState({ result: x })
    }
    if(this.state.operator==='%'){
      let x = this.state.num1 % this.state.num2;
      this.setState({ result: x })
    }
   
  }
  render() {
    return (
      <div className="calculate">
        <form>
          <p>Input Numbers:</p>
          <input type="text" onChange={this.handleInputChange} name="num1"/>
          <br />
          <br />
          <input type="text" onChange={this.handleInputChange} name="num2" />
          <br />
          <br />
          <label>
            <select value={this.state.operator} onChange={this.handleChange}>
              <option value="+">+</option>
              <option value="-">-</option>
              <option value="*">*</option>
              <option value="/">/</option>
              <option value="%">%</option>
            </select>
          </label>
          <br />
          <br />
          <button onClick={this.actionHandler} type="submit" > calculate </button>
          <p>Result History</p>
          <input type="text" value={this.state.result} readOnly />
        </form>
      </div>
    );
  }
}

export default App;
