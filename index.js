import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//import ToDoList from './ToDoList';
//import ArrayMap from './ArrayMap';
//import About from './About';
//import StateProp from './StateProp';

/*const myfirstelement = <h1>Hello React!</h1>
    const myelement = <input type="text" />;

ReactDOM.render(myfirstelement, document.getElementById('root'));*/

ReactDOM.render( 
      <div>
        <App/>
      </div>,
      document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
